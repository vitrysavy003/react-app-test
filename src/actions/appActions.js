import { appActions } from '../const/types';

export const loadingAction = () => ({type: appActions.LOADING });
export const loadedAction = () => ({type: appActions.LOADED});
export const changeLanguageCode = (code) => ({
  type: appActions.LAGUAGE_CHANGED,
  code: code
});

