import { loadedAction, loadingAction } from "./appActions";
import {
  getNotes as apiGetNotes,
  updateNote as apiUpdateNote,
  createNote as apiCreateNote,
  deleteNote as apiDeleteNote
} from '../data/api';
import { noteActions } from "../const/types";

export const getNotes = () =>
  (dispatch) => {
    dispatch(loadingAction());
    apiGetNotes()
      .then(notes => {
        dispatch({
          type: noteActions.GET,
          notes: notes
        });
        dispatch(loadedAction());
      });
  }

export const setActiveNoteId = (noteId) => ({
  type: noteActions.SET_ACTIVE,
  noteId: noteId
});

export const updateNote = (note) =>
  (dispatch) => {
    dispatch({
      type: noteActions.UPDATE,
      note: note
    });
    apiUpdateNote(note);
  };

export const addNote = (note) =>
  (dispatch) => {
    apiCreateNote(note)
      .then(newNote => {
        console.log('newNote is ' + JSON.stringify(newNote));
        dispatch({
          type: noteActions.ADD,
          note: newNote
        });
      });
  };

export const deleteNote = (note) =>
  (dispatch) => {
    apiDeleteNote(note);
    dispatch({
      type: noteActions.DELETE,
      note: note
    });
  };