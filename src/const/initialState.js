export default Object.freeze({
  loading: false,
  notes: [],
  activeNoteId: null,
  languageCode: null
});