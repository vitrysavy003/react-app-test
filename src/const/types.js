export const appActions = Object.freeze({
  LOADING: Symbol('appActions:loading'),
  LOADED: Symbol('appActions:loaded'),
  LAGUAGE_CHANGED: Symbol('AppActions:languageChanged')
});

export const noteActions = Object.freeze({
  GET: Symbol('noteActions:get'),
  UPDATE: Symbol('noteActions:update'),
  ADD: Symbol('noteActions:create'),
  DELETE: Symbol('noteActions:delete'),
  SET_ACTIVE: Symbol('noteActions:setActive')
});
