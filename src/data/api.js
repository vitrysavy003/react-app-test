import Note from "./note";

const baseUrl = "https://firestore.googleapis.com/v1beta1/projects/test-5fdde/databases/(default)/documents/notes"
const headers = Object.freeze({
  "Content-Type" : "application/json"
});

/**
 * Getting all notes
 */
export const getNotes = () => {
  const url = `${baseUrl}?orderBy=created`;
  return fetch(url, {
    method: 'GET',
    headers: headers,
  })
  .then(response => {
    //TODO handle errors
    return response.json();
  })
  .then(data => 
    data.documents? 
      data.documents.map(dbNote => Note.fromDbNote(dbNote)) :
      []
  )
}

/**
 * Creating new note 
 * @param {Note} note
 */
export const createNote = (note) => {
  return fetch(baseUrl, {
    method: 'POST',
    mode: "cors",
    headers: headers,
    body: JSON.stringify(Note.toDbNote(note))
  })
  .then(response => {
    //TODO handle errors
    return response.json();
  })
  .then(data => {
    return Note.fromDbNote(data);
  });
}

/**
 * Deleting note
 * @param {Note} note
 */
export const deleteNote = (note) => {
  const url = `${baseUrl}/${note.id}`;
  return fetch(url, {
    method: 'DELETE',
    mode: "cors",
    headers: headers
  })
  .then(() => {
    //TODO handling you know what
    return;
  });
}

/**
 * Update note
 * @param {Note} note
 */
export const updateNote = (note) => {
  console.log('here');
  const url = `${baseUrl}/${note.id}`;
  return fetch(url, {
    method: 'PATCH',
    mode: "cors",
    headers : headers,
    body: JSON.stringify(Note.toDbNote(note))
  })
  .then(() => {
    //TODO handling errors
    return;
  });
}




