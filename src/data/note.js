const PREFIX = 'projects/test-5fdde/databases/(default)/documents/notes/';
/** 
 * @typedef note 
 * @property {string} id
 * @property {string} title
 */
export default class Note {
  constructor() {
    this.id = '';
    this.title = '';
  }

  /** @param {note} note */
  static toDbNote(note) {
    return {
      name: note.id? `${PREFIX}{note.Id}`: null,
      fields: {
        title: {
          stringValue: note.title
        },
        created: {
          integerValue: Date.now()
        }
      }
    };
  }

  /** @returns {note} */
  static fromDbNote(dbNote){
    return {
      id: dbNote.name.replace(PREFIX, ''),
      title: dbNote.fields.title.stringValue
    }
  }

}
