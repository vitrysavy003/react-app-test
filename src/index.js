import 'promise-polyfill/src/polyfill';
import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/js/bootstrap.bundle.js'
import App, {appRouteStates} from './ui/App';
import { getStore } from './store/store';
import { Provider } from 'react-redux';
import { LocalizeProvider } from "react-localize-redux";
import { changeLanguageCode } from './actions/appActions';
import { UIRouter, UIView, pushStateLocationPlugin } from '@uirouter/react';

const store = getStore();

const states = [
  {
    name: 'home',
    url: '/:langId',
    component: () => <App/>,
    resolve: [{
      token: 'lang',
      deps: ['$transition$'],
      resolveFn: (trans) => {
        const param = decodeURIComponent(trans.params().langId) || 'en'
        getStore().dispatch(changeLanguageCode(param));
      }
    }]
  }
].concat(appRouteStates);

const plugins = [pushStateLocationPlugin];

ReactDOM.render(
  <Provider store={store}>
    <LocalizeProvider store={store}>
      <UIRouter plugins={plugins} states={states}>
        <UIView/>
      </UIRouter>
    </LocalizeProvider>
  </Provider>
  ,
  document.getElementById('root'));