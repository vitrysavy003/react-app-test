import { appActions } from '../const/types'
import initialState from '../const/initialState';

export const languageReducer = (state = initialState.languageCode, action) => {
  switch (action.type) {
    case appActions.LAGUAGE_CHANGED:
      return action.code;
    default:
      return state;
  }
}
