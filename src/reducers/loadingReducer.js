import { appActions } from '../const/types'
import initialState from '../const/initialState';

export default (state = initialState.loading, action) => {
  switch (action.type) {
    case appActions.LOADING:
      return true;
    case appActions.LOADED:
      return false;
    default:
      return state;
  }
};