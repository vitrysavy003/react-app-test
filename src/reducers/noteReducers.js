import { noteActions } from '../const/types'
import initialState from '../const/initialState';

export const notesReducer = (state = initialState.notes, action) => {
  let nextState = null;
  switch (action.type) {
    case noteActions.GET: {
      return action.notes;
    }
    case noteActions.ADD: {
      nextState = Array.from(state);
      nextState.push(action.note);
      return nextState;
    }
    case noteActions.DELETE: {
      const { note } = action;
      nextState = state.filter(item => !note.id || (item.id !== note.id));
      return nextState;
    }
    case noteActions.UPDATE: {
      const { note } = action;
      nextState = Array.from(state);
      const index = nextState.findIndex(item => item.id === note.id);
      nextState[index] = note;
      return nextState;
    }
    default:
      return state;
  }
}

export const activeNoteReducer = ( state = initialState.activeNoteId, action) => {
  switch(action.type){
    case noteActions.SET_ACTIVE: {
      return action.noteId;
    }
    default:
      return state;
  }
}
