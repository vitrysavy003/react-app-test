import { combineReducers } from 'redux';
import { notesReducer, activeNoteReducer } from './noteReducers';
import loadingReducer from './loadingReducer';
import { localizeReducer } from 'react-localize-redux';
import { languageReducer } from './languageReducer';

const rootReducer = combineReducers({
  activeNoteId: activeNoteReducer,
  notes: notesReducer,
  loading: loadingReducer,
  languageCode: languageReducer,
  localize: localizeReducer,
});

export default rootReducer;