import { createStore, compose, applyMiddleware } from 'redux';
import rootReducer from '../reducers/rootReducer';
import thunk from 'redux-thunk';
import initialState from '../const/initialState';

let store = null;
export const getStore = () =>
  store || (store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(thunk)
      //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
  ));
