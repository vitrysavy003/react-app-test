import React, { Component } from 'react';
import Modal, { ModalBody, ModalFooter } from './Modal';
import PropTypes from 'prop-types';
import { getStore } from '../store/store';
import { addNote } from '../actions/noteActions'
import { UIRouterConsumer } from '@uirouter/react';
import { Translate } from 'react-localize-redux';



class AddModal extends Component {
  constructor() {
    super();

    this.state = {
      title: ''
    }
    this.onSave = this.onSave.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.$form = React.createRef();
  }

  onSave(event) {
    const $form = this.$form.current;
    const valid = $form.checkValidity();
    $form.classList.add('was-validated');
    if (valid === true) {
      const newNote = { title: this.state.title };
      getStore().dispatch(addNote(newNote));
    }
    event.preventDefault();
    this.props.router.stateService.go('home');
  }

  onCancel(event) {
    event.preventDefault();
    this.props.router.stateService.go('home');
  }

  onChange(event) {
    const $form = this.$form.current;
    const newValue = event.target.value;
    this.setState({
      title: newValue
    });
    requestAnimationFrame(() => {
      $form.checkValidity();
      $form.classList.add('was-validated');
    });
  }

  render() {
    return (
      <Translate>
        {({translate}) =>
          <Modal title={translate('TITLE_ADD')} onCancel={this.onCancel}>
            <form ref={this.$form} className="needs-validation" noValidate onSubmit={this.onSave}>
              <ModalBody>
                <div className='form-group'>
                  <label>{translate('FIELD_TITLE')}</label>
                  <input type="text" autoFocus className="form-control" required maxLength="250" placeholder={translate('PLACEHOLDER_ADD')} value={this.state.title} onChange={this.onChange} />
                  <div className="invalid-feedback">
                    {translate('LABEL_REQUIRED')}
                  </div>
                </div>
              </ModalBody>
              <ModalFooter onCancel={this.onCancel}>
                <button type="submit" className="btn btn-primary">{translate('BUTTON_ADD')}</button>
              </ModalFooter>
            </form>
          </Modal>
        }
      </Translate>
    );
  }

  static get propertyTypes() {
    return {
      router: PropTypes.object.isRequired
    }
  }
}

export default (props) =>
  (
    <UIRouterConsumer>
      {router =>
        <AddModal router={router} />
      }
    </UIRouterConsumer>
  );