import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Navbar from './Navbar';
import Footer from './Footer';
import { getStore } from '../store/store';
import { getNotes, setActiveNoteId } from '../actions/noteActions'
import { withLocalize, Translate } from 'react-localize-redux';
import { renderToStaticMarkup } from "react-dom/server";
import translations from '../translations/translations.json';
import Row from './Row';
import HeaderRow from './HeaderRow';
import EditModal from './EditModal';
import AddModal from './AddModal';
import { UIView, UISref } from '@uirouter/react';

export const appRouteStates = [
  {
    name: 'home.edit',
    url: '/:noteId',
    component: () => <EditModal />
    ,
    resolve: [{
      token: 'note',
      deps: ['$transition$'],
      resolveFn: (trans) => {
        const param = decodeURIComponent(trans.params().noteId);
        getStore().dispatch(setActiveNoteId(param));
      }
    }],
    onExit: (trans, state) => {
      getStore().dispatch(setActiveNoteId(null));
    }
  },
  {
    name: 'home.create',
    url: '/create',
    component: () => <AddModal />
  }
];


class App extends Component {
  render() {
    return (
      <Fragment>
        <Navbar>
        </Navbar>
        <main role="main" className="container position-relative">
          <UIView />
          <table className="table table-hover">
            <thead>
              <HeaderRow />
            </thead>
            <tbody>
              {!this.props.notes.length &&
                <tr>
                  <td colSpan="2" className='text-muted'><Translate id='LABEL_NO_DATA'></Translate></td>
                </tr>
              }
              {this.props.notes.map(note =>
                <Row key={note.id} note={note} />
              )}
            </tbody>
          </table>
        </main>
        <Footer >
          <div className="text-muted d-flex">
            <div className="pl-2">
              <UISref to="home" params={{ langId: 'en' }}>
                <a>English</a>
              </UISref>
            </div>
            <div className="pl-2">
              <UISref to="home" params={{ langId: 'cs' }}>
                <a>Česky</a>
              </UISref>
            </div>
          </div>
        </Footer>
      </Fragment >
    )
  }

  componentDidMount() {
    //load data
    getStore().dispatch(getNotes());
  }

  constructor(props) {
    super(props);
    this.props.initialize({
      languages: translations.languages,
      options: { renderToStaticMarkup }
    });
    this.props.addTranslationForLanguage(translations.cs, 'cs');
    this.props.addTranslationForLanguage(translations.en, 'en');
    this.props.setActiveLanguage(this.props.languageCode);
  }
  static get propertyTypes() {
    return {
      notes: PropTypes.array.isRequired
    }
  }

  static mapStateToProps(state) {
    const { notes, languageCode } = state;
    return { notes, languageCode };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.languageCode !== this.props.languageCode) {
      this.props.setActiveLanguage(this.props.languageCode);
    }
  }
}

export default withLocalize(connect(App.mapStateToProps)(App))