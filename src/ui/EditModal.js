import React, { Component } from 'react';
import Modal, { ModalBody, ModalFooter } from './Modal';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getStore } from '../store/store';
import { updateNote, deleteNote } from '../actions/noteActions'
import { UIRouterConsumer } from '@uirouter/react';
import { Translate } from 'react-localize-redux';



class EditModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: ''
    }
    this.onSave = this.onSave.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.$form = React.createRef();
  }

  componentDidMount() {
    this._updateTitle();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.note !== this.props.note) {
      this._updateTitle();
    }
  }

  _updateTitle() {
    const title = (this.props.note) ? this.props.note.title : '';
    this.setState({
      title: title
    });
  }

  onSave(event) {
    const $form = this.$form.current;
    const valid = $form.checkValidity();
    $form.classList.add('was-validated');
    if (valid === true) {
      const newNote = Object.assign({}, this.props.note, { title: this.state.title })
      getStore().dispatch(updateNote(newNote));
    }
    event.preventDefault();
    this.props.router.stateService.go('home');
  }

  onDelete(event) {
    getStore().dispatch(deleteNote(Object.assign({}, this.props.note)));
    event.preventDefault();
    this.props.router.stateService.go('home');
  }

  onCancel(event) {
    event.preventDefault();
    this.props.router.stateService.go('home');
  }

  onChange(event) {
    const $form = this.$form.current;
    const newValue = event.target.value;
    this.setState({
      title: newValue
    });
    requestAnimationFrame(() => {
      $form.checkValidity();
      $form.classList.add('was-validated');
    });
  }

  render() {
    return (
      <Translate>
        {({translate}) =>
          <Modal title={translate('TITLE_UPDATE')} onCancel={this.onCancel}>
            <form ref={this.$form} className="needs-validation" noValidate onSubmit={this.onSave}>
              <ModalBody>
                <div className='form-group'>
                  <label>{translate('FIELD_TITLE')}</label>
                  <input type="text" autoFocus className="form-control" required maxLength="250" placeholder={translate('PLACEHOLDER_UPDATE')} value={this.state.title} onChange={this.onChange} />
                  <div className="invalid-feedback">
                    {translate('LABEL_REQUIRED')}
                  </div>
                </div>
              </ModalBody>
              <ModalFooter onCancel={this.onCancel}>
                <button type="button" onClick={this.onDelete} className="btn btn-outline-danger">{translate('BUTTON_DELETE')}</button>
                <button type="submit" className="btn btn-primary">{translate('BUTTON_UPDATE')}</button>
              </ModalFooter>
            </form>
          </Modal>
        }
      </Translate>
    );
  }

  static get propertyTypes() {
    return {
      note: PropTypes.object.isRequired,
      router: PropTypes.object.isRequired
    }
  }

  static mapStateToProps(state) {
    const { notes, activeNoteId } = state;
    return {
      note: notes.find(note => note.id === activeNoteId)
    }
  }
}

const ConnectedEditModal = connect(EditModal.mapStateToProps)(EditModal);

export default (props) =>
  (
    <UIRouterConsumer>
      {router =>
        <ConnectedEditModal router={router} />
      }
    </UIRouterConsumer>
  );