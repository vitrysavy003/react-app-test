import React from 'react';

const Footer = (props) => (
  <footer className="footer bg-light border-top fixed-bottom">
    <div className="container">
    {props.children}
    </div>
  </footer>
)

export default Footer;

