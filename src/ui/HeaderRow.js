import React from 'react';
import { connect } from 'react-redux';
import { UISref } from '@uirouter/react';
import { Translate } from 'react-localize-redux';


const HeaderRow = (props) =>
  (
    <tr className="notes-row">
      <th> Notes </th>
      <th className="d-flex flex-row-reverse">
        <UISref to="home.create" params={{ langId: props.languageCode }}>
          <a className="btn btn-primary active" role="button" aria-pressed="true">
           <Translate id='BUTTON_ADD'></Translate> 
          </a>
        </UISref>
      </th>
    </tr>
  );

HeaderRow.mapStateToProps = (state) => {
  const { languageCode } = state;
  return { languageCode };
}

export default connect(HeaderRow.mapStateToProps)(HeaderRow);