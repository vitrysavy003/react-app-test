import React, { Component } from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import { Translate } from 'react-localize-redux';

class Modal extends Component {
  constructor() {
    super();
    this.$rootElement = React.createRef();

  }
  render() {
    return (
      <Translate>
        {({ translate }) =>
          <div ref={this.$rootElement}>
            <div className="modal fade" tabIndex="-1" role="dialog">
              <div className="modal-dialog" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">{this.props.title}</h5>
                    <button type="button" className="close" onClick={this.props.onCancel} data-dismiss="modal" aria-label={translate('BUTTON_CLOSE')}>
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  {this.props.children}
                </div>
              </div>
            </div>
          </div>
        }
      </Translate>
    );
  }

  componentDidMount() {
    const $dialog = this.$rootElement.current.querySelector('[role=dialog]');
    requestAnimationFrame(() => {
      $($dialog).modal('show');
    });
  }

  componentWillUnmount() {
    const $dialog = this.$rootElement.current.querySelector('[role=dialog]');
    requestAnimationFrame(() => {
      $($dialog).modal('hide');
    });
  }

  static get propertyTypes() {
    return {
      onCancel: PropTypes.func.isRequired
    }
  }
}

export default Modal;

export const ModalBody = (props) =>
  (<div className="modal-body">
    {props.children}
  </div>);

export const ModalFooter = (props) =>
  (<div className="modal-footer">
    {props.children}
    <button type="button" className="btn btn-secondary" onClick={props.onCancel} data-dismiss="modal"><Translate id='BUTTON_CLOSE'/></button>
  </div>);

ModalFooter.propertyTypes = {
  onCancel: PropTypes.func.isRequired
};
