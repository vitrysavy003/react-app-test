import React from 'react';

const Navbar = (props) => (
  <nav className="navbar navbar-expand-md navbar-light bg-light fixed-top border-bottom">
    <a className="navbar-brand" href="#">NoteIt.rn</a>
    <div className="collapse navbar-collapse d-flex flex-row-reverse" id="navbarCollapse">
      {props.children}
    </div>
  </nav >
);

export default Navbar;
