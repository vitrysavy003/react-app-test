import React from 'react';
import { connect } from 'react-redux';
import { UISref } from '@uirouter/react'
import { Translate } from 'react-localize-redux';

const Row = (props) => {
  const note = props.note;
  const params = {
    langId: props.languageCode,
    noteId: note.id
  };
  return (
    <tr className="notes-row">
      <td>{note.title}</td>
      <td className="d-flex flex-row-reverse">
        <UISref className="notes-edit" to="home.edit" params={params}>
          <a>
            <Translate id='BUTTON_UPDATE'></Translate>
          </a>
        </UISref>
      </td>
    </tr>
  );
}
export default connect((state) => ({ languageCode: state.languageCode }))(Row);