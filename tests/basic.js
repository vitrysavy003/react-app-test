describe('homepage', function(){
  it('should be running', function(){
    browser.waitForAngularEnabled(false);
    browser.get('http://localhost:9000');
    expect(browser.getTitle()).toEqual('NoteIt.rn');
  });
});